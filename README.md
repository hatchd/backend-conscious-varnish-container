# Build  the container
> docker build -t blah .

# Run the container on port 80, point it towards 2x backend servers hosted on your own machine
```
docker run \
    -e BACKENDS='192.168.1.2:8080,192.168.1.2:8081' \
    -e ALLOWED_PURGE_CIDR_RANGES='"172.0.0.0"/8; "192.168.0.0"/8;' \
    -p 80:80 \
    -v /tmp/varnish-logs/:/var/log/varnish
    blah
```

# Test the PURGE capability
> curl -XPURGE localhost:80