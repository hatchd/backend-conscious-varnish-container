python3 template.py

LOG_FORMAT='%t %{HOST}i Method="%m", URI="%U%q", ClientAddr="%h", X-Forwarded-For="%{X-Forwarded-For}i", Proto="%H", Size="%b", Status="%s", Referrer="%{Referer}i", UserAgent="%{User-agent}i", Time="%D", HitMiss="%{Varnish:hitmiss}x", Content-Type="%{Content-Type}o"'

# Wait 5 seconds for Varnish to start, run varnishnsca which logs to a file
(sleep 5; varnishncsa -b -c -F "${LOG_FORMAT}" -a -w /var/log/varnish/varnishncsa.log) &

exec /usr/sbin/varnishd -f /etc/varnish/default.vcl -a 0.0.0.0:80 -s malloc,1G -F