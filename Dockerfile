FROM alpine:3.10

# Install varnish
RUN apk add varnish

# Install Python3 and the Mako template library
RUN apk add python3
RUN pip3 install mako

# Copy our default.vcl template file across
COPY default.vcl.tmpl /tmp/default.vcl.tmpl
# Copy our template replacement script across
COPY template.py template.py

# Copy across entry script
COPY entry.sh entry.sh
RUN chmod +x entry.sh

# Entry point to run varnish
ENTRYPOINT "./entry.sh"